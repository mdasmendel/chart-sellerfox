
var app = angular.module('app', [
  'pascalprecht.translate',
  'ui.bootstrap',
  'ngRoute',
  'googlechart'
]);

app.run(function ($rootScope) {
  // adds some basic utilities to the $rootScope for debugging purposes
  $rootScope.log = function (thing) {
    console.log(thing);
  };

  $rootScope.alert = function (thing) {
    alert(thing);
  };
});

app.config(function($translateProvider) {

  $translateProvider.preferredLanguage('en');
  $translateProvider.fallbackLanguage('en');

  $translateProvider.useStaticFilesLoader({
    prefix: 'translations/',
    suffix: '.json'
  });


});