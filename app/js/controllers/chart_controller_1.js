angular.module('app').controller("ChartController1", function ($scope, StatisticService, $routeParams, $templateCache, $interpolate, $translate) {

  var currentYear = new Date().getFullYear();

  $scope.selectedYear = currentYear;

  var currentMonth = new Date().getMonth();

  var lang = $routeParams.language;

  moment.locale(lang);

  $translate.use(lang);

  var temp = ['v', 'sdf', 'v', 'sdf', 'v', 'sdf', 'v', 'sdf', 'v', 'sdf', 'v', 'sdf'];

  console.log();

  $scope.sliderConfig = {
    bounds: { min: new Date($scope.selectedYear, 0, 1), max: new Date($scope.selectedYear, 11, 31, 12, 59, 59) },
    prev: function () {
      $scope.selectedYear--;
      updateConfig();
      getPage();
    },
    next: function () {
      if ($scope.selectedYear < currentYear) {
        $scope.selectedYear++;
        updateConfig();
        getPage();
      }
    },
    scales: [
      {
        first: function (value) {
          return value;
        },
        end: function (value) {
          return value;
        },
        next: function (value) {
          var next = new Date(value);
          return new Date(next.setMonth(value.getMonth() + 1));
        },
        label: function (value) {
          return temp[value.getMonth()];
        },
        format: function (tickContainer, tickStart, tickEnd) {
          tickContainer.addClass("myCustomClass");
        }
      }
    ]
  };


  var params = {
    id: $routeParams.clientId,
    select: '*',
    where: ' datum >= "' + $scope.selectedYear + '-01-01" and datum <= "' + $scope.selectedYear + '-12-31"',
    offset: '0',
    limit: '1000'
  };

  function updateConfig() {
    var min = new Date($scope.selectedYear, 10, 1);
    var max = new Date($scope.selectedYear, 11, 31, 12, 59, 59);

    if (currentYear === $scope.selectedYear) {
      max = Date.now();
      if (currentMonth > 1) {
        min = new Date(moment().subtract(2, 'month'));
      } else {
        min = new Date($scope.selectedYear, 0, 1, 0, 0, 0);
      }
    }
    $scope.selectedRange = {min: min, max: max};

    $scope.sliderConfig.bounds = { min: new Date($scope.selectedYear, 0, 1), max: new Date($scope.selectedYear, 11, 31, 12, 59, 59) };

    params.where = ' datum >= "' + $scope.selectedYear + '-01-01" and datum <= "' + $scope.selectedYear + '-12-31"';
  }
  var z = 0;
  updateConfig();


  var chart1 = {
    type: "AreaChart",
    data: {"cols": [
      {id: "date", label: "Date", type: "string"},
      {id: "statistic", label: "Statistik", type: "number"},
      {type: 'string', role: 'tooltip', 'p': {'html': true, 'role': 'tooltip'}}
    ],
      "rows": []
    },
    options: {
      tooltip: {
        isHtml: true
      },
      "isStacked": "true",
      "fill": 20,
      "displayExactValues": false,
      "vAxis": {
//      "title": "Statistik",
        "gridlines": {
          "count": 10
        },
        viewWindow: {
          min: 0,
          max: 'auto'
        },
        format: '#'
      },
      "hAxis": {
//        format:"MMM d, y",
//      title: "Date",
        gridlines: {
          "count": 10
        }
      },
      pointSize: 5,
      legend: {
        position: "none"
      },
      pointShape: 'circle'
    }
  };


  chart1.formatters = {

    "date": [{
      columnNum: 0,
      formatType: 'long'
    }]
  };

  $scope.chart = chart1;


  var getPage = function () {
    $scope.dataRow = [];
    StatisticService.getStatisticData(params)
      .success(function (result) {
        var obj = {};

        for (var i = 0; i < result.length; i++) {
          obj[result[i].datum] = result[i];
        }

        var date = moment($scope.selectedYear - 1 + '-12-31');
        var final = [], dateStr;

        do {
          dateStr = date.add(1, 'days').format('YYYY-MM-DD');
          final.push(obj[dateStr] || {datum: dateStr});
        } while (dateStr !== $scope.selectedYear + '-12-31');

        $scope.dataRow = final;

        createStatistik();
      });
  };

  var createStatistik = function () {
    var rows = [];
    for (var i = 0; i < $scope.dataRow.length; i++) {
      var date = new Date($scope.dataRow[i].datum);
      if (date >= $scope.selectedRange.min && date <= $scope.selectedRange.max) {
        var dd = date.getDate();
        var mm = date.getMonth();
        var yyyy = date.getFullYear();
        date = new Date(yyyy, mm, dd, 0);
        statistic = {
          datum: moment(new Date($scope.dataRow[i].datum)).format("MMMM D, YYYY"),
          visits: parseInt($scope.dataRow[i].auction_visites || 0, 10),
          visitors: parseInt($scope.dataRow[i].auction_visitors || 0, 10),
          oneClick: parseInt($scope.dataRow[i].one_click || 0, 10),
          moreClicks: parseInt($scope.dataRow[i].more_clicks || 0, 10),
          clicksGallery: parseInt($scope.dataRow[i].clicks_gallery || 0, 10),
          traffic: parseInt($scope.dataRow[i].traffic || 0, 10)
        };

//        console.log(statistic.datum);

        var statisticString = $interpolate($templateCache.get('chart/partials/statistic_label_template.html'))(statistic);

        var statisticCount = statistic.visits + statistic.visitors + statistic.oneClick + statistic.moreClicks + statistic.clicksGallery;
//        var dateF = moment(new Date($scope.dataRow[i].datum)).format("MMM D, YYYY");
        rows.push({
          c: [
            {v: moment(new Date($scope.dataRow[i].datum)).format("MMM D, YYYY")},
            {v: statisticCount},
            {v: statisticString,"p": {}}
          ]
        });
      }
    }
    chart1.data.rows = rows;
    if (rows.length > 12){
      var showEvery = parseInt(rows.length / 12, 10);
      chart1.options.hAxis.showTextEvery = showEvery;
    }
  };

  getPage();

  $scope.$watch('selectedRange', function (newV, oldV) {
    var newDay = {
      min: (new Date(newV.min)).getDay(),
      max: (new Date(newV.max)).getDay()
    };
    var oldDay = {
      min: (new Date(oldV.min)).getDay(),
      max: (new Date(oldV.max)).getDay()
    };
    if (oldDay.min !== newDay.min || oldDay.max !== newDay.max) {
      createStatistik();
    }
  });

});