angular.module('app').controller("ChartController2", function ($scope, StatisticService, $routeParams) {

  var currentYear = new Date().getFullYear();

  $scope.selectedYear = currentYear;

  var currentMonth = new Date().getMonth();

  $scope.sliderConfig = {
    bounds: { min: new Date($scope.selectedYear, 0, 1), max: new Date($scope.selectedYear, 11, 31, 12, 59, 59) },
    prev: function () {
      $scope.selectedYear--;
      updateConfig();
      getPage();
    },
    next: function () {
      if ($scope.selectedYear < currentYear) {
        $scope.selectedYear++;
        updateConfig();
        getPage();
      }
    }
  };

  var params = {
    id: $routeParams.clientId,
    select: '*',
    where: ' datum >= "' + $scope.selectedYear + '-01-01" and datum <= "' + $scope.selectedYear + '-12-31"',
    offset: '0',
    limit: '1000'
  };

  function updateConfig(){
    var min = new Date($scope.selectedYear, 10, 1);
    var max = new Date($scope.selectedYear, 11, 31, 12, 59, 59);

    if(currentYear === $scope.selectedYear){
      max = Date.now();
      if (currentMonth > 1) {
        min = new Date(moment().subtract(2, 'month'));
      } else {
        min = new Date($scope.selectedYear, 0, 1, 0, 0, 0);
      }
    }
    $scope.selectedRange = {min: min, max: max};

    $scope.sliderConfig.bounds = { min: new Date($scope.selectedYear, 0, 1), max: new Date($scope.selectedYear, 11, 31, 12, 59, 59) };

    params.where = ' datum >= "' + $scope.selectedYear + '-01-01" and datum <= "' + $scope.selectedYear + '-12-31"';
  }

  updateConfig();


  var chart1 = {};
  chart1.type = "AreaChart";
  chart1.displayed = false;
  chart1.data = [
    ['Date', 'Sales', {type: 'string', role: 'tooltip', 'p': {'html': true}}],
    ['2013',  1000,'sdf'],
    ['2014',  1170,'sdf'],
    ['2015',  660, 'dfg'],
    ['2016',  1030,'<h1>dfghsdfg</h1>']
  ];
//  chart1.data.addRows([
//    ['2000',  94, '',  '', 58, 28],
//    ['2004', 101, '',  '', 63, 30],
//    ['2008', 110, 'An all time high!', '<img width=100px src="http://upload.wikimedia.org/wikipedia/commons/2/28/Flag_of_the_USA.svg">', 100, 47],
//    ['2012', 104, '',  '', 88, 65]
//  ]);
  chart1.options = {
//    "title": "Statistik",
    tooltip: {
      isHtml: true
//      ,trigger: 'focus'
    },
    "isStacked": "true",
    "fill": 20,
    "displayExactValues": false,
    "vAxis": {
//      "title": "Statistik",
      "gridlines": {
        "count": 10
      },
      viewWindow: {
        min: 0,
        max: 'auto'
      },
      format: '#'
    },
    "hAxis": {
//      title: "Date",
      gridlines: {
        "count": 10
      }
    },
    pointSize: 5,
    legend: {
      position: "none"
    },
    pointShape: 'circle'
  };


  var formatCollection = [
    {
      name: "date",
      format: [
        {
          columnNum: 1,
          formatType: 'Medium'
        }
      ]
    }
  ];


  chart1.formatters = {};

  $scope.chart = chart1;


  var getPage = function () {
    $scope.dataRow = [];
    StatisticService.getStatisticData(params)
      .success(function (result) {
        var obj = {};

        for (var i = 0; i < result.length; i++) {
          obj[result[i].datum] = result[i];
        }

        var date = moment($scope.selectedYear - 1 + '-12-31');
        var final = [], dateStr;

        do {
          dateStr = date.add(1, 'days').format('YYYY-MM-DD');
          final.push(obj[dateStr] || {datum: dateStr});
        } while (dateStr !== $scope.selectedYear + '-12-31');

        $scope.dataRow = final;

        createStatistik();
      });
  };


  var createStatistik = function () {
    var rows = [];
    for (var i = 0; i < $scope.dataRow.length; i++) {
      var date = new Date($scope.dataRow[i].datum);
      if (date >= $scope.selectedRange.min && date <= $scope.selectedRange.max) {
        var dd = date.getDate();
        var mm = date.getMonth();
        var yyyy = date.getFullYear();
        date = new Date(yyyy, mm, dd, 0);

        var visites = parseInt($scope.dataRow[i].auction_visites || 0, 10);
        var visitors = parseInt($scope.dataRow[i].auction_visitors || 0, 10);
        var oneClick = parseInt($scope.dataRow[i].one_click || 0, 10);
        var moreClicks = parseInt($scope.dataRow[i].more_clicks || 0, 10);
        var clicksGallery = parseInt($scope.dataRow[i].clicks_gallery || 0, 10);
        var traffic = parseInt($scope.dataRow[i].traffic || 0, 10);

        var statisticString = '\nAuktionsaufrufe:  ' + visites +
          '\nEindeutige Besucher: ' + visitors +
          '\nBesucher mit Auktionsaufrufen = 1: ' + oneClick +
          '\nBesucher mit Auktionsaufrufen > 1: ' + moreClicks +
          '\nGalerie-Klicks: ' + clicksGallery +
          '\nGalerie-Traffic: ' + traffic + '%';

        var statisticCount = visites + visitors + oneClick + moreClicks + clicksGallery + traffic;
        rows.push({
          c: [
            {v: date},
            {v: statisticCount, f:statisticString}
          ]
        });
      }
    }
    chart1.data.rows = rows;
  };

  getPage();

  $scope.$watch('selectedRange', function () {
    createStatistik();
  });

});