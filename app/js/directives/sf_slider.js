
angular.module('app').directive('sfSlider', function ($timeout) {
  var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"];

  var sfSliderDefault = {
    bounds: {min: new Date(new Date().getFullYear(), 0, 1), max: new Date(new Date().getFullYear(), 11, 31, 12, 59, 59)},
    scales: [{
      first: function(value){ return value; },
      end: function(value) {return value; },
      next: function(value){
        var next = new Date(value);
        return new Date(next.setMonth(value.getMonth() + 1));
      },
      label: function(value) {
        return months[value.getMonth()];
      },
      format: function(tickContainer, tickStart, tickEnd){
        tickContainer.addClass("myCustomClass");
      }
    }]
  };

  var handler = function(scope, element, attrs) {
    var options = angular.extend({}, sfSliderDefault, scope.$eval(attrs.sfSlider));
    scope.options = options;

    var sliderElement = $(element).find('.slider-holder');

    var scopeAttributes = ['prevText', 'nextText'];

    var oldValue = {};

    var init = function() {
      if (init === angular.noop) {
        return;
      }

      sliderElement.dateRangeSlider(options);

      sliderElement.on("valuesChanging", function(e, data) {
        oldValue = scope[attrs.range] = data.values;
        scope.$apply();
      });

      init = angular.noop;
    };

    scope.$watch(attrs.sfSlider, function(newVal) {

      init();
      if(newVal !== undefined) {
        sliderElement.dateRangeSlider('option', newVal);
      }
    }, true);



    scope.$watch(attrs.range, function(newValue) {
      init();
      if (!angular.equals(oldValue, newValue)) {
        sliderElement.dateRangeSlider('values', newValue.min, newValue.max);
      }
    }, true);

//    element.bind('$destroy', function() {
//      alert('destroy');
//    });

    angular.forEach(scopeAttributes, function(attr) {
      // support {{}} and watch for updates
      attrs.$observe(attr, function(newVal) {
        scope[attr] = newVal;
      });
    });

    $timeout(init, 0, true);
  };

  return {
    restrict: 'A',
    link: handler,
    templateUrl: 'directives/sf_slider.html'
  };
});