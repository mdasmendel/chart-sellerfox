angular.module("app").config(function($routeProvider, $locationProvider) {

  //$locationProvider.html5Mode(true);

  $routeProvider.when('/:language/chart/:clientId', {
    templateUrl: 'chart/chart.html',
    controller: 'ChartController'
  });

});
